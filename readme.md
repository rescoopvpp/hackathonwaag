# Install a Python venv and install requirements

```bash
python3 -m venv venv
source venv/bin/activate
pip install -U pip wheel
pip install -r requirements.txt
```

# Start Jupyter

```bash
source venv/bin/activate
jupyter notebook
```

# Start the COFYMocker

```bash
source venv/bin/activate
python cofymocker.py 
```
