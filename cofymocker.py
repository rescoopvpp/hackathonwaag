# -*- coding: utf-8 -*-
"""
Created on Tue May 18 23:50:07 2021

This script reads in a influx db dump file containing real-life measurements, then loops through
all the measurements, comparing the HH:MM:SS timestamp of the measurement to the current timestamp.
If the measurement matches one of the measurements defined in the list, it builds a MQTT payload
according to COFY standards and fires it to the broker. This way, it mimics a COFY box in a real-life environment.
If using a 24H measurement file, this script will keep 'replaying' and sending the measurements continously.

@author: Joannes //Epyon// Laveyne
"""

import pandas as pd
import numpy as np
import json
import os
import paho.mqtt.client as mqtt
from datetime import datetime
import time
import logging

import signal

from utils import to_number

prevtimestamp = 0

MQTT_BROKER = os.getenv("MQTT_BROKER", "localhost")


class GracefulDeath:
    """
    When the Grim Sysadmin comes to reap with his scythe,
    let this venerable daemon process die a Graceful Death
    """
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


"""
MQTT configuration vars
Could be moved to config file if needed
"""
client = mqtt.Client("utility_meter")
brokers_out = {"broker1": MQTT_BROKER}
client.connect(brokers_out["broker1"])
"""
Read in datafile, set timestamp as index
"""
df = pd.read_csv("data.csv")
df = df.drop(
    ["assumed_state", "device_class_str", "domain", "entity_id_str", "icon_str"],
    axis=1,
    errors="ignore",
)
df = df.loc[df["name"] != "name"]
df["time"] = df["time"].apply(pd.to_numeric).astype(np.int64)
df["time"] = df["time"].div(1000000000).astype(np.int64)
df["datetime"] = pd.to_datetime(df["time"], unit="s")
df.set_index("datetime", inplace=True)


def valid(telegram: dict):
    required_keys = [
        "metric",
        "metricKind",
        "unit",
        "value",
        "timestamp",
        "entity",
        "friendly_name",
    ]
    if set(required_keys) <= set(telegram.keys()):
        return True
    else:
        return False


"""
Main loop, runs until terminated (SIGKILL or keyboard interrupt)
"""
if __name__ == "__main__":
    killer = GracefulDeath()
    while not killer.kill_now:
        try:
            """
            Every second, try to select a row with the same HH:MM:SS timestamp as now
            """
            timestamp = int(time.time())
            if prevtimestamp != timestamp:
                date_time_obj = datetime.utcfromtimestamp(timestamp)
                try:
                    rowData = df.loc[
                        df.index.strftime("%M:%S") == date_time_obj.strftime("%M:%S")
                    ]

                    for index, row in rowData.iterrows():
                        topic = ""
                        telegram = {}
                        telegram["channel"] = row["entity_id"]
                        topic = "data/devices/"
                        if row["entity_id"] == "total_active_power":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["entity"] = "utility_meter"
                            telegram["metric"] = "GridElectricityPower"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Active power consumption"
                            telegram["flags"] = "totals"
                        elif row["entity_id"] == "total_energy_consumed":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["entity"] = "utility_meter"
                            telegram["metric"] = "GridElectricityImport"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy consumed"
                            telegram["flags"] = "totals"
                        elif row["entity_id"] == "total_energy_injected":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["entity"] = "utility_meter"
                            telegram["metric"] = "GridElectricityExport"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy injected"
                            telegram["flags"] = "totals"
                        elif row["entity_id"] == "active_power_consumption":
                            continue
                        elif row["entity_id"] == "active_power_injection":
                            continue
                        elif row["entity_id"] == "voltage_phase_l1" or row["entity_id"] == "voltage_phase_1":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["metric"] = "GridElectricityVoltage"
                            telegram["entity"] = "utility_meter"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Voltage phase 1"
                        elif row["entity_id"] == "total_gas_consumption":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["entity"] = "utility_meter"
                            telegram["metric"] = "naturalGasImport"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Total gas consumption"
                        elif row["entity_id"] == "total_injection_day":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["entity"] = "utility_meter"
                            telegram["metric"] = "electricityExport"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy injected day"
                        elif row["entity_id"] == "total_injection_night":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["entity"] = "utility_meter"
                            telegram["metric"] = "electricityExport"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy injected night"
                        elif row["entity_id"] == "total_consumption_day":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["entity"] = "utility_meter"
                            telegram["metric"] = "electricityImport"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy consumed day"
                        elif row["entity_id"] == "total_consumption_night":
                            topic = topic + "utility_meter/" + row["entity_id"]
                            telegram["entity"] = "utility_meter"
                            telegram["metric"] = "electricityImport"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy consumed night"
                        elif row["entity_id"] == "sb2000_energy":
                            topic = "data/devices/pv/sb2000/total_energy_injected"
                            telegram["entity"] = "sb2000"
                            telegram["metric"] = "PvElectricityProduction"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy produced"
                        elif row["entity_id"] == "sb2000_power":
                            topic = "data/devices/pv/sb2000/active_power"
                            telegram["entity"] = "sb2000"
                            telegram["metric"] = "PvElectricityPower"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Active power production"
                        elif row["entity_id"] == "sb2000_voltage":
                            topic = "data/devices/pv/sb2000/voltage"
                            telegram["entity"] = "sb2000"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Voltage phase 1"
                            telegram["metric"] = "PvElectricityVoltage"
                        elif row["entity_id"] == "sbs2500_energy":
                            topic = "data/devices/bess/sb2500/total_energy_injected"
                            telegram["entity"] = "sbs2500"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy injected"
                            telegram["metric"] = "BatteryElectricityExport"
                        elif row["entity_id"] == "sbs2500_power":
                            topic = "data/devices/bess/sb2500/active_power"
                            telegram["entity"] = "sbs2500"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Active power"
                            telegram["metric"] = "BatteryElectricityPower"
                        elif row["entity_id"] == "sbs2500_battery_state":
                            topic = "data/devices/bess/sb2500/battery_state"
                            telegram["entity"] = "sbs2500"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Battery state"
                            telegram["metric"] = "BatteryElectricitySoc"
                        elif row["entity_id"] == "hp_energy":
                            topic = "data/devices/hp/vitocal/total_energy"
                            telegram["entity"] = "vitocal"
                            telegram["metricKind"] = "cumulative"
                            telegram["friendly_name"] = "Total energy"
                            telegram["metric"] = "HpElectricityConsumption"
                        elif row["entity_id"] == "hp_power":
                            topic = "data/devices/hp/vitocal/active_power"
                            telegram["entity"] = "vitocal"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Active power"
                            telegram["metric"] = "HpElectricityPower"
                        elif row["entity_id"] == "hp_temperature_buffer":
                            topic = "data/devices/hp/vitocal/temperature_buffer"
                            telegram["entity"] = "vitocal"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Buffer temperature"
                            telegram["metric"] = "DHWTankTemperature"
                        elif row["entity_id"] == "hp_operation_mode_text":
                            topic = "data/devices/hp/vitocal/operation_mode_text"
                            telegram["entity"] = "vitocal"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Heat pump operation mode"
                            telegram["metric"] = "State"
                        elif row["entity_id"] == "cal1_total_energy":
                            topic = "data/devices/heat/calorimeter"
                            telegram["entity"] = "calorimeter"
                            telegram["metricKind"] = "gauge"
                            telegram["metric"] = "finalHeatConsumption"
                            telegram["friendly_name"] = "Final heat consumption"
                        elif row["entity_id"] == "outsidetemp":
                            topic = "data/devices/thermostat/outside"
                            telegram["entity"] = "thermostat"
                            telegram["metricKind"] = "gauge"
                            telegram["metric"] = "OutdoorTemperature"
                            telegram["friendly_name"] = "Outside temperature"
                        elif row["entity_id"] == "sww_uit_buffer_nr_ketel":
                            topic = "data/devices/heater/gasboiler/feedwatertemp"
                            telegram["entity"] = "gasboiler"
                            telegram["metricKind"] = "gauge"
                            telegram[
                                "friendly_name"
                            ] = "Gas boiler feedwater temperature"
                        elif row["entity_id"] == "sww_na_ketel":
                            topic = "data/devices/heater/gasboiler/dhwtemp"
                            telegram["entity"] = "gasboiler"
                            telegram["metricKind"] = "gauge"
                            telegram["friendly_name"] = "Gas boiler DHW temperature"

                        telegram["value"] = to_number(row["value"])
                        telegram["unit"] = row["name"]
                        telegram["timestamp"] = timestamp
                        if valid(telegram):
                            logging.debug("valid telegram")
                            logging.debug(telegram)
                            payload = json.dumps(telegram)
                            client.publish(topic, payload)
                        else:
                            logging.warning("invalid telegram")
                            logging.warning(telegram)
                except Exception as ex:
                    logging.exception(ex)
                    pass
            prevtimestamp = timestamp
        except KeyboardInterrupt:
            logging.debug("Keybaord interrupt received")
            logging.debug("Disconnecting from broker... ", end="")
            client.disconnect()
            pass
    # Make a graceful exit when this daemon gets terminated
    logging.debug("Termination signal received")
    logging.debug("Disconnecting from broker... ", end="")
    client.disconnect()
    logging.debug("done")
    logging.debug("Goodbye, cruel Unix world")
